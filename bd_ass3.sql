DROP DATABASE IF EXISTS TP_ORDER_SYSTEM;
CREATE DATABASE IF NOT EXISTS TP_ORDER_SYSTEM;

USE TP_ORDER_SYSTEM;

CREATE TABLE IF NOT EXISTS Customer
(
	id int unique NOT NULL auto_increment primary key,
	firstName varchar(20) NOT NULL ,
    lastName varchar(20) NOT NULL ,
    age int NOT NULL,
    email varchar(40),
    address varchar(40)
);

CREATE TABLE IF NOT EXISTS orders
(
	id int unique auto_increment primary key NOT NULL ,
	customer_id int NOT NULL,
    order_date datetime NOT NULL,
    FOREIGN KEY (customer_id) REFERENCES Customer(id)
);

CREATE TABLE IF NOT EXISTS Product
(
	id int unique auto_increment primary key NOT NULL ,
	name varchar(20),
    description varchar(20),
    price float NOT NULL ,
    quantity int NOT NULL
);

CREATE TABLE IF NOT EXISTS OrderProducts
(
	id int unique auto_increment primary key NOT NULL,
	order_id int,
    product_id int,
    quantity int NOT NULL, 
    FOREIGN KEY (order_id) REFERENCES Orders(id), 
	FOREIGN KEY (product_id) REFERENCES Product(id)
);

DROP TRIGGER IF EXISTS customerDeletion;

DELIMITER //
CREATE TRIGGER customerDeletion BEFORE DELETE ON Customer
FOR EACH ROW
BEGIN
		IF (OLD.id is not NULL) THEN
			DELETE FROM OrderProducts where OrderProducts.order_id IN (SELECT Orders.id FROM Orders WHERE Orders.customer_id = OLD.id); 
			DELETE FROM orders WHERE Orders.customer_id = OLD.id;
		END IF;
END //
delimiter ;

DROP TRIGGER IF EXISTS productsDeletion;
DELIMITER //
CREATE TRIGGER productsDeletion BEFORE DELETE ON product
FOR EACH ROW
BEGIN
		IF (OLD.id is not NULL) THEN
			DROP TEMPORARY TABLE IF EXISTS table2;
			CREATE TEMPORARY TABLE table2 (SELECT order_id FROM OrderProducts WHERE OrderProducts.product_id = OLD.id);
			DELETE FROM OrderProducts WHERE OrderProducts.product_id = OLD.id; 
            DELETE FROM OrderProducts WHERE order_id IN (SELECT * FROM table2);
            DELETE FROM Orders WHERE id IN (SELECT * FROM table2);
            DROP TEMPORARY TABLE table2;
        END IF;
END //
delimiter ;

DROP VIEW IF EXISTS OrdersINFO;

CREATE VIEW OrdersINFO AS
SELECT O.id, O.order_date, O.customer_id, C.firstName, C.lastName
FROM Orders O
INNER JOIN Customer C ON C.id = O.customer_id;

DROP VIEW IF EXISTS OrderItems;

CREATE VIEW OrderItems AS
SELECT O.product_id, P.name, P.price, O.quantity, O.order_id
FROM OrderProducts O
INNER JOIN Product P ON P.id = O.product_id;

