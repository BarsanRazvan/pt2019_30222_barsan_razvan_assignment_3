package dao;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.DriverJDBC;

public class AbstractDAO<T> {

	protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

	private final Class<T> type;

	@SuppressWarnings("unchecked")
	public AbstractDAO() {
		this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	private String createSelectQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM ");
		sb.append(type.getSimpleName());
		if (field != null)
			sb.append(" WHERE " + field + " =?");
		return sb.toString();
	}

	private List<T> createObjects(ResultSet resultSet) {
		List<T> list = new ArrayList<>();

		try {
			while (resultSet.next()) {
				T instance = type.newInstance();
				for (Field field : type.getDeclaredFields()) {
					Object value = resultSet.getObject(field.getName());
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
					Method method = propertyDescriptor.getWriteMethod();
					method.invoke(instance, value);
				}
				list.add(instance);
			}
		} catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException
				| InvocationTargetException | SQLException | IntrospectionException e) {
			e.printStackTrace();
		}
		return list;
	}

	public List<T> findAll() {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery(null);
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(query);
			resultSet = statement.executeQuery();
			return createObjects(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return new ArrayList<>();
	}

	private String createInsertQuery(T object) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ");
		sb.append(type.getSimpleName());
		sb.append(" (");
		Field[] fields = type.getDeclaredFields();
		String prefix = " ";
		for (int i = 1; i < fields.length; i++) {
			sb.append(prefix);
			sb.append(fields[i].getName());
			prefix = ", ";
		}
		sb.append(" ) VALUES (");
		prefix = " ";
		for (int i = 1; i < fields.length; i++) {
			fields[i].setAccessible(true);
			try {
				sb.append(prefix);
				prefix = ", ";
				sb.append("'");
				Object value = fields[i].get(object);
				sb.append(value.toString());
				sb.append("'");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		sb.append(") ");
		System.out.println(sb.toString());
		return sb.toString();
	}

	public boolean insert(T object) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createInsertQuery(object);
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(query);
			statement.execute();
			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return false;
	}

	private String createDeleteQuery(String field) {
		StringBuilder sb = new StringBuilder();
		sb.append("DELETE FROM ");
		sb.append(type.getSimpleName());
		sb.append(" WHERE id = ?");
		System.out.println(sb.toString());
		return sb.toString();
	}

	public boolean delete(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createDeleteQuery("id");
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return false;
	}

	private String createUpdateQuery(T object) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE ");
		sb.append(type.getSimpleName());
		sb.append(" SET ");
		Field[] fields = type.getDeclaredFields();
		String prefix = " ";
		for (int i = 1; i < fields.length; i++) {
			sb.append(prefix);
			sb.append(fields[i].getName());
			sb.append(" = ");
			Object value;
			try {
				fields[i].setAccessible(true);
				value = fields[i].get(object);
				sb.append("'");
				sb.append(value.toString());
				sb.append("'");
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
			prefix = ", ";
		}
		sb.append(" WHERE ID = ");
		fields[0].setAccessible(true);
		try {
			sb.append(fields[0].get(object));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

	public boolean update(T object) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createUpdateQuery(object);
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(query);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:update " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return false;
	}
	
	public T findById(int id) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createSelectQuery("id");
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(query);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return createObjects(resultSet).get(0);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, type.getName() + "DAO:findAll " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return null;
	}
}
