package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import connection.DriverJDBC;
import model.OrderItems;


public class OrderItemsDAO{
	
	private List<OrderItems> getItems(ResultSet rs){
		List<OrderItems> items = new ArrayList<>();
		OrderItems item;
		try {
			while (rs.next()) {
				item = new OrderItems();
				item.setProductId(rs.getInt("product_id"));
				item.setProductName(rs.getString("name"));
				item.setPrice(rs.getFloat("price"));
				item.setQuantity(rs.getInt("quantity"));
				items.add(item);
			}
		} catch (SQLException e) {
			e.getMessage();
			e.printStackTrace();
		}
		return items;
	}
	
	public List<OrderItems> findById(int id){
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String queryOrders = "SELECT * FROM OrderItems WHERE order_id = ?";
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(queryOrders);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			return getItems(resultSet);
		} catch (SQLException e) {
			e.getMessage();
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return null;
	}
}
