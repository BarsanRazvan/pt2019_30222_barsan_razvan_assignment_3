package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import connection.DriverJDBC;
import model.OrderItems;
import model.Orders;

public class OrderDAO extends AbstractDAO<Orders>{
	
	private String createInsertQueryOrders(Orders order) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO Orders (customer_id, order_date) VALUES(");
		sb.append("'" + order.getClientID() + "', ");
		sb.append("'" + order.getDate() + "')");
		return sb.toString();
	}
	
	private String createInsertQueryOrderProducts(OrderItems item, int orderID) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ORDERPRODUCTS (order_id, product_id, quantity) VALUES( '" + orderID + "', ");
		sb.append("'" + item.getProductId() + "', ");
		sb.append("'" + item.getQuantity() + "')");
		return sb.toString();
	}
	
	@Override
	public boolean insert(Orders order) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String query = createInsertQueryOrders(order);
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(query, new String[] {"id"});
			statement.execute();
			ArrayList<OrderItems> items = (ArrayList<OrderItems>) order.getItems();
			ResultSet rs = statement.getGeneratedKeys();
			int orderID;
			if(rs.next()) {
				orderID = rs.getInt(1);
				for(OrderItems item : items) {
					query = createInsertQueryOrderProducts(item, orderID);
					statement = connection.prepareStatement(query);
					statement.execute();
				}
				return true;
			} else return false;
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:insert " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return false;
	}
	
	

	private List<Orders> createOrders(ResultSet rs) {
		List<Orders> orders = new ArrayList<>();
		Orders order;
		OrderItemsDAO itemsDAO = new OrderItemsDAO();
		try {
			while (rs.next()) {
				order = new Orders();
				order.setId(rs.getInt("id"));
				order.setDate(rs.getDate("order_date"));
				order.setClientID(rs.getInt("customer_id"));
				order.setCustomerFirstName(rs.getString("firstName"));
				order.setCustomerLastName(rs.getString("lastName"));
				order.setItems(itemsDAO.findById(order.getId()));
				orders.add(order);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:createOrders " + e.getMessage());
			e.printStackTrace();
		}
		return orders;
	}
	
	@Override
	public List<Orders> findAll(){
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		String queryOrders = "SELECT * FROM OrdersINFO";
		try {
			connection = DriverJDBC.getConnection();
			statement = connection.prepareStatement(queryOrders);
			resultSet = statement.executeQuery();
			return createOrders(resultSet);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			DriverJDBC.close(resultSet);
			DriverJDBC.close(statement);
			DriverJDBC.close(connection);
		}
		return null;
	}

}
