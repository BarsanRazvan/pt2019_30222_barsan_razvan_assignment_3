package model;

import java.util.Date;
import java.util.List;

public class Orders {
	private int id;
	private Date date;
	private int clientID;
	private String customerFirstName;
	private String customerLastName;
	private List<OrderItems> items;
	private float total;

	public Orders() {
		
	}
	
	public Orders(int id, Date date, int clientID, String customerFirstName, String customerLastName, List<OrderItems> items){
		this.id = id;
		this.date = date;
		this.clientID = clientID;
		this.customerFirstName = customerFirstName;
		this.customerLastName = customerLastName;
		this.items = items;
		for(OrderItems item : items) {
			total += item.getPrice() * item.getQuantity();
		}
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date orderDate) {
		this.date = orderDate;
	}

	public String getCustomerFirstName() {
		return customerFirstName;
	}

	public void setCustomerFirstName(String customerFirstName) {
		this.customerFirstName = customerFirstName;
	}

	public String getCustomerLastName() {
		return customerLastName;
	}

	public void setCustomerLastName(String customerLastName) {
		this.customerLastName = customerLastName;
	}
	
	public List<OrderItems> getItems() {
		return items;
	}

	public void setItems(List<OrderItems> items) {
		this.items = items;
		this.total = 0;
		for(OrderItems item : items) {
			total += item.getPrice() * item.getQuantity();
		}
	}
	
	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	@Override
	public String toString() {
		String s = "Order \r\nId = " + id + "\r\nCustomer Name = " + customerLastName + " " + customerFirstName
					  + "\r\nProducts: =";
		for(OrderItems item: items) {
			s += " " + item.toString();
		}
		s += "\r\nTotal = " + String.format("%.2f", total); 
		s += "";
		return s;
	}
}
