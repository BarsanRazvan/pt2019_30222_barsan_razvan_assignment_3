package model;

public class Customer {
	private int id;
	private String firstName;
	private String lastName;
	private int age;
	private String email;
	private String address;

	public Customer() {
		
	}
	
	public Customer(int id, String firstName, String lastName, int age, String email, String address) {
		this.id = id;
		this.age = age;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.address = address;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Customer [ id = " + id + ", first name = " + firstName + ", last name = " + lastName + ", age = " + age
						 + ", email = " + email + ", address = " + address + "]";
	}
}
