package model;

public class OrderItems {
	private int productId;
	private String productName;
	private float price;
	private int quantity;
	
	public OrderItems() {
	}
	
	public OrderItems(String product, int quantity, float price, int productId){
		this.productName = product;
		this.quantity = quantity;
		this.price = price;
		this.productId = productId;
	}
	
	public int getProductId() {
		return productId;
	}

	public void setProductId(int id) {
		this.productId = id;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return productName + ", quantity = " + quantity + ", price = " + price;
	}
}
