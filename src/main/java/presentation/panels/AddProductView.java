package presentation.panels;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddProductView extends JFrame{
	private static final long serialVersionUID = -2444104160756481897L;
	private JTextField name;
	private JTextField description;
	private JTextField price;
	private JTextField quantity;
	private JButton addButton;
	private JButton cancelButton;
	
	public AddProductView() {
		init();
		addKeyListeners();
		this.setLayout(new GridLayout(5, 2));
		this.add(new JLabel("Name: "));
		this.add(name);
		this.add(new JLabel("Description: "));
		this.add(description);
		this.add(new JLabel("Price: "));
		this.add(price);
		this.add(new JLabel("quantity"));
		this.add(quantity);
		this.add(addButton);
		this.add(cancelButton);
		this.setLocationRelativeTo(null);
		this.setTitle("Add new product");
		this.setSize(500,170);
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	private void init() {
		name = new JTextField(20);
		description = new JTextField(20);
		price= new JTextField(20);
		quantity = new JTextField(20);
		addButton = new JButton("ADD");
		cancelButton = new JButton("CANCEL");
	}
	
	public List<String> getNewProduct(){
		List<String> client = new ArrayList<>(); 
		client.add(name.getText());
		client.add(description.getText());
		client.add(price.getText());
		client.add(quantity.getText());
		return client;
	}
	
	private void addKeyListeners() {
		KeyListener key = new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent ke){ 
				char c = ke.getKeyChar(); 
				if((!(Character.isDigit(c))) && (c != '\b') ) 
					ke.consume(); 
			} 
			public void keyReleased(KeyEvent e){} 
			public void keyPressed(KeyEvent e){} 
			};
		price.addKeyListener(key);
		quantity.addKeyListener(key);
	}
	
	public void showDialogBox(String message) {
		JOptionPane.showMessageDialog(this, message);
	}
	
	public void addListener(ActionListener ac) {
		addButton.addActionListener(ac);
	}
	
	public void cancelListener(ActionListener ac) {
		cancelButton.addActionListener(ac);
	}
}
