package presentation.panels;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class PlaceOrder extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JComboBox<String> client;
	private JComboBox<String> product1;
	private JTextField quantity1;
	private JComboBox<String> product2;
	private JTextField quantity2;
	private JComboBox<String> product3;
	private JTextField quantity3;
	private JButton placeButton;
	private JButton cancelButton;
	List<String> products;
	
	public PlaceOrder(List<String> products, List<String> customers) {
		init();
		this.products = products;
		this.setLayout(new GridLayout(5, 3));
		addItems(products, customers);
		addKeyListeners();
		this.add(new JLabel("Client"));
		this.add(client);
		this.add(new JLabel(""));
		this.add(new JLabel("Product 1:"));
		this.add(product1);
		this.add(quantity1);
		this.add(new JLabel("Product 2:"));
		this.add(product2);
		this.add(quantity2);
		this.add(new JLabel("Product 3:"));
		this.add(product3);
		this.add(quantity3);
		this.add(placeButton);
		this.add(cancelButton);
		this.setLocationRelativeTo(null);
		this.setTitle("Place new order");
		this.setSize(500,170);
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	private void addItems(List<String> products, List<String> customers) {
		product1.addItem("No product selected");
		product2.addItem("No product selected");
		product3.addItem("No product selected");
		for(String item : products) {
			product1.addItem(item);
			product2.addItem(item);
			product3.addItem(item);
		}
		for(String item : customers) {
			client.addItem(item);
		}
		
	}
	
	private void addKeyListeners() {
		KeyListener key = new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent ke){ 
				char c = ke.getKeyChar(); 
				if((!(Character.isDigit(c))) && (c != '\b') ) 
					ke.consume(); 
			} 
			public void keyReleased(KeyEvent e){} 
			public void keyPressed(KeyEvent e){} 
			};
		quantity1.addKeyListener(key);
		quantity2.addKeyListener(key);
		quantity3.addKeyListener(key);
	}

	private void init() {
		client = new JComboBox<>();
		product1 = new JComboBox<>();
		product2 = new JComboBox<>(); 
		product3 = new JComboBox<>();
		quantity1 = new JTextField(5);
		quantity2 = new JTextField(5);
		quantity3 = new JTextField(5);
		placeButton = new JButton("PLACE");
		cancelButton = new JButton("CANCEL");
	}
	
	public void placeListener(ActionListener ac) {
		placeButton.addActionListener(ac);
	}
	
	public void cancelListener(ActionListener ac) {
		cancelButton.addActionListener(ac);
	}
	
	public void showDialogBox(String message) {
		JOptionPane.showMessageDialog(this, message);
	}
	
	public List<String> getProducts() {
		List<String> order = new ArrayList<>();
		StringTokenizer multiTokenizer = new StringTokenizer(product1.getSelectedItem().toString(), " ");
		order.add(multiTokenizer.nextToken());
		order.add(multiTokenizer.nextToken());
		order.add(multiTokenizer.nextToken());
		order.add(quantity1.getText());
		multiTokenizer = new StringTokenizer(product2.getSelectedItem().toString(), " ");
		order.add(multiTokenizer.nextToken());
		order.add(multiTokenizer.nextToken());
		order.add(multiTokenizer.nextToken());
		order.add(quantity2.getText());
		multiTokenizer = new StringTokenizer(product3.getSelectedItem().toString(), " ");
		order.add(multiTokenizer.nextToken());
		order.add(multiTokenizer.nextToken());
		order.add(multiTokenizer.nextToken());
		order.add(quantity3.getText());
		return order;
	}
	
	public List<String> getClient() {
		List<String> client = new ArrayList<>();
		StringTokenizer multiTokenizer = new StringTokenizer(this.client.getSelectedItem().toString(), " ");
		client.add(multiTokenizer.nextToken());
		client.add(multiTokenizer.nextToken());
		client.add(multiTokenizer.nextToken());
		return client;
	}
}
