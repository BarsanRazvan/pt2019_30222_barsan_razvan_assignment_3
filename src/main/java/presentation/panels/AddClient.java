package presentation.panels;

import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddClient extends JFrame {
	private static final long serialVersionUID = -2444104160756481897L;
	private JTextField firstName;
	private JTextField lastName;
	private JComboBox<Integer> age;
	private JTextField email;
	private JTextField address;
	private JButton addButton;
	private JButton cancelButton;
	
	public AddClient() {
		init();
		for(int i = 18; i<80; i++) {
			age.addItem(i);
		}
		this.setLayout(new GridLayout(6, 2));
		this.add(new JLabel("First Name"));
		this.add(firstName);
		this.add(new JLabel("Last Name"));
		this.add(lastName);
		this.add(new JLabel("Age: "));
		this.add(age);
		this.add(new JLabel("email:"));
		this.add(email);
		this.add(new JLabel("address"));
		this.add(address);
		this.add(addButton);
		this.add(cancelButton);
		this.setLocationRelativeTo(null);
		this.setTitle("Add new customer");
		this.setSize(500,170);
		this.setResizable(false);
		this.setAlwaysOnTop(true);
		this.setVisible(true);
	}
	
	private void init() {
		firstName = new JTextField(20);
		lastName = new JTextField(20);
		age = new JComboBox<>();
		email = new JTextField(20);
		addButton = new JButton("ADD");
		address = new JTextField(20);
		cancelButton = new JButton("CANCEL");
	}
	
	public List<String> getNewClient(){
		List<String> client = new ArrayList<>(); 
		client.add(firstName.getText());
		client.add(lastName.getText());
		client.add(age.getSelectedItem().toString());
		client.add(email.getText());
		client.add(address.getText());
		return client;
	}
	
	public void showDialogBox(String message) {
		JOptionPane.showMessageDialog(this, message);
	}
	
	public void addListener(ActionListener ac) {
		addButton.addActionListener(ac);
	}
	
	public void cancelListener(ActionListener ac) {
		cancelButton.addActionListener(ac);
	}
}
