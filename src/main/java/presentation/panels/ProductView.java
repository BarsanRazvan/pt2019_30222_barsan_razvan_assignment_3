package presentation.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

public class ProductView extends JPanel {
	private static final long serialVersionUID = 2399834676856383952L;
	private JButton addProduct;
	private JButton editProduct;
	private JButton deleteProduct;
	private JButton showAllProducts;
	private JPanel buttonPanel;
	private JTable products;
	private JScrollPane scroll;
	private JLabel productsLabel;

	public ProductView() {
		init();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		productsLabel.setFont(new Font("Serif", Font.BOLD, 20));
		products.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.add(productsLabel);
		this.add(scroll);
		this.add(buttonPanel);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(showAllProducts);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(addProduct);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(editProduct);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(deleteProduct);
		buttonPanel.add(Box.createHorizontalGlue());
	}

	private void init() {
		addProduct = new JButton("Add Product");
		editProduct = new JButton("Edit Product");
		deleteProduct = new JButton("Delete Product");
		showAllProducts = new JButton("Show All Products");
		buttonPanel = new JPanel();
		products = new JTable();
		scroll = new JScrollPane(products, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		productsLabel = new JLabel("Products");
	}
	
	public void updateProductsTable(TableModel model) {
		products.setModel(model);
	}
	
	public void addProductActionListener(ActionListener ac) {
		addProduct.addActionListener(ac);
	}
	
	public void addEditActionListener(ActionListener ac) {
		editProduct.addActionListener(ac);
	}
	
	public void addDeleteActionListener(ActionListener ac) {
		deleteProduct.addActionListener(ac);
	}
	
	public void addShowAllActionListener(ActionListener ac) {
		showAllProducts.addActionListener(ac);
	}
	
	public List<String> getSelectedProduct(){
		List<String> client = new ArrayList<>();
		int row = products.getSelectedRow();
		for(int i = 0 ; i < products.getColumnCount(); i++) {
			client.add((String)products.getValueAt(row, i));
		}
		return client;
	}
}
