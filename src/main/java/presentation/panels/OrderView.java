package presentation.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

public class OrderView extends JPanel{
	private static final long serialVersionUID = 2399834676856383952L;
	private JButton placeOrder;
	private JButton showAllOrders;
	private JPanel buttonPanel;
	private JTable orders;
	private JScrollPane scroll;
	private JLabel ordersLabel;
	
	public OrderView() {
		init();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		ordersLabel.setFont(new Font("Serif", Font.BOLD, 20));
		orders.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		orders.setEnabled(false);
		this.add(Box.createVerticalGlue());
		this.add(ordersLabel);
		this.add(scroll);
		this.add(buttonPanel);
		this.add(Box.createVerticalGlue());
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(showAllOrders);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(placeOrder);
		buttonPanel.add(Box.createHorizontalGlue());
	}

	private void init() {
		placeOrder = new JButton("Place Order");
		showAllOrders = new JButton("Show All Orders");
		buttonPanel = new JPanel();
		orders = new JTable();
		scroll = new JScrollPane(orders, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		ordersLabel = new JLabel("Orders");
	}
	
	public void updateOrdersTable(TableModel model) {
		orders.setModel(model);
	}
	
	public void addOrderActionListener(ActionListener ac) {
		placeOrder.addActionListener(ac);
	}

	public void addShowAllActionListener(ActionListener ac) {
		showAllOrders.addActionListener(ac);
	}
}
