package presentation.panels;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableModel;

public class ClientView extends JPanel {
	private static final long serialVersionUID = 1L;
	private JButton addClient;
	private JButton editClient;
	private JButton deleteClient;
	private JButton showAllClients;
	private JPanel buttonPanel;
	private JTable clients;
	private JScrollPane scroll;
	private JLabel clientLabel;

	public ClientView() {
		init();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		clientLabel.setFont(new Font("Serif", Font.BOLD, 20));
		clients.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		clients.getTableHeader().setReorderingAllowed(false);
		this.add(clientLabel);
		this.add(scroll);
		this.add(buttonPanel);
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
		buttonPanel.add(Box.createHorizontalGlue());
		buttonPanel.add(showAllClients);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(addClient);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(editClient);
		buttonPanel.add(Box.createRigidArea(new Dimension(20,0)));
		buttonPanel.add(deleteClient);
		buttonPanel.add(Box.createHorizontalGlue());
	}

	private void init() {
		addClient = new JButton("Add Client");
		editClient = new JButton("Edit Client");
		deleteClient = new JButton("Delete Client");
		showAllClients = new JButton("Show All Clients");
		buttonPanel = new JPanel();
		clients = new JTable();
		scroll = new JScrollPane(clients, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		clientLabel = new JLabel("Customers");
	}
	
	public void updateClientsTable(TableModel model) {
		clients.setModel(model);
	}
	
	public void addClientActionListener(ActionListener ac) {
		addClient.addActionListener(ac);
	}
	
	public void addEditActionListener(ActionListener ac) {
		editClient.addActionListener(ac);
	}
	
	public void addDeleteActionListener(ActionListener ac) {
		deleteClient.addActionListener(ac);
	}
	
	public void addShowAllActionListener(ActionListener ac) {
		showAllClients.addActionListener(ac);
	}
	
	public List<String> getSelectedClient(){
		List<String> client = new ArrayList<>();
		int row = clients.getSelectedRow();
		for(int i = 0 ; i < clients.getColumnCount(); i++) {
			client.add((String)clients.getValueAt(row, i));
		}
		return client;
	}
}
