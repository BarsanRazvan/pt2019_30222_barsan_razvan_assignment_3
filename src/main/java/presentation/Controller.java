package presentation;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import bll.CustomerBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Customer;
import model.Orders;
import model.OrderItems;
import model.Product;
import presentation.panels.AddClient;
import presentation.panels.AddProductView;
import presentation.panels.PlaceOrder;

public class Controller {
	private View view;
	private CustomerBLL customerBLL;
	private OrderBLL orderBLL;
	private ProductBLL productBLL;
	public Controller() {
		view = new View();
		customerBLL = new CustomerBLL();
		orderBLL = new OrderBLL();
		productBLL = new ProductBLL();
		addCustomerListeners();
		addProductListeners();
		addOrderListeners();
	}
	
	private void addCustomerListeners() {
		view.showAllCustomersActionListener(ev ->{
			List<Object> objects = new ArrayList<>(customerBLL.findAllCustomers());
			if(!objects.isEmpty())
				view.updateCustomerTable(createTable(objects));
			else view.showDialogBox("Customer list currently empty!");
		});
		
		view.deleteCustomerActionListener(ev ->{
			ArrayList<String> client = (ArrayList<String>) view.getSelectedCustomer();
			customerBLL.deleteCustomer(Integer.parseInt(client.get(0)));
			view.showDialogBox("Customer deleted!");
		});
		
		view.editCustomerActionListener(ev ->{
			ArrayList<String> client = (ArrayList<String>) view.getSelectedCustomer();
			Customer customer = new Customer(Integer.parseInt(client.get(0)), client.get(1), client.get(2),
											 Integer.parseInt(client.get(3)), client.get(4), client.get(5));
			customerBLL.updateCustomer(customer);
			view.showDialogBox("Customer edited!");
		});
		
		view.addCustomerActionListener(ev ->{
			AddClient add = new AddClient();
			add.addListener(e -> {
				ArrayList<String> client = (ArrayList<String>) add.getNewClient();
				Customer newClient = new Customer(0, client.get(0), client.get(1), Integer.parseInt(client.get(2)),
											  client.get(3), client.get(4));
				if(customerBLL.insertCustomer(newClient))
					add.showDialogBox("Customer added!");
				else add.showDialogBox("Error! Could not add customer!");
			});
			add.cancelListener(e-> {
				add.dispose();
			});
		});
	}
		
	private void addProductListeners() {
		view.showAllProductsActionListener(ev ->{
			List<Object> objects = new ArrayList<>(productBLL.findAllProducts());
			if(!objects.isEmpty())
				view.updateProductTable(createTable(objects));
			else view.showDialogBox("Product list currently empty!");
		});
		
		view.deleteProductActionListener(ev ->{
			ArrayList<String> client = (ArrayList<String>) view.getSelectedProduct();
			productBLL.deleteProduct(Integer.parseInt(client.get(0)));
			view.showDialogBox("Product deleted!");
		});
		
		view.editProductActionListener(ev ->{
			ArrayList<String> client = (ArrayList<String>) view.getSelectedProduct();
			Product product = new Product(Integer.parseInt(client.get(0)), client.get(1), client.get(2), Float.parseFloat(client.get(3)), 
										 Integer.parseInt(client.get(4)));
			productBLL.updateProduct(product);
			view.showDialogBox("Product edited!");
		});
		
		view.addProductActionListener(ev ->{
			AddProductView add = new AddProductView();
			add.addListener(e -> {
				ArrayList<String> product = (ArrayList<String>) add.getNewProduct();
				Product newProduct = new Product(0, product.get(0), product.get(1), Float.parseFloat(product.get(2)),
												Integer.parseInt(product.get(3)));
				if(productBLL.insertProduct(newProduct))
					add.showDialogBox("Product added!");
				else add.showDialogBox("Error! Could not add product!");
			});
			add.cancelListener(e-> {
				add.dispose();
			});
		});
	}
	
	
	private void addOrderListeners() {
		view.showAllOrdersActionListener(ev ->{
		List<Object> objects = new ArrayList<>(orderBLL.findAllOrders());
		if(!objects.isEmpty()) {
			view.updateOrderTable(createTable(objects));
		}else view.showDialogBox("Order list currently empty!");
		});
	
		view.placeOrderActionListener(ev ->{
			PlaceOrder order = new PlaceOrder(getProductNames(), getCustomerNames());
			order.placeListener(e -> {
				ArrayList<String> products = (ArrayList<String>) order.getProducts();
				ArrayList<String> customer = (ArrayList<String>) order.getClient();
				List<OrderItems> items = new ArrayList<>();
				boolean itemSelected = false;
				boolean quantityOk = true;
				for(int i = 0; i< 12; i=i+4) {
					if(!products.get(i).equals("No")) {
						if(products.get(i+3).equals("")) {
							order.showDialogBox("Please insert quantity!");
							quantityOk = false;
							break;
						} else 	{
							itemSelected = true;
							items.add(new OrderItems(products.get(i+1), Integer.parseInt(products.get(i+3)), 
													 Float.parseFloat(products.get(i+2)), Integer.parseInt(products.get(i))));
						}
					}
				}
				if(quantityOk) {
					if(itemSelected) {
						List<Product> addedProducts = new ArrayList<>();
						List<Integer> changedQuantity = new ArrayList<>();
						Orders newOrder = new Orders(1, new Timestamp(System.currentTimeMillis()), Integer.parseInt(customer.get(0)), 
													 customer.get(1), customer.get(2), items);
						boolean understock = false;
						for(OrderItems item : items) {
							Product product = productBLL.findProductById(item.getProductId());
							addedProducts.add(product);
							changedQuantity.add(item.getQuantity());
							if(item.getQuantity() > product.getQuantity()) {
								understock = true;
								break;
							}
						}
						if(!understock)
							if(orderBLL.placeOrder(newOrder)) {
								updateStock(addedProducts, changedQuantity);
								writeResult(newOrder);
								order.showDialogBox("Order placed!");
							}
							else order.showDialogBox("Invalid order!");
						else order.showDialogBox("Understock");
					} else {
						order.showDialogBox("Please select products!");
					}
				}
			});
				order.cancelListener(e-> {
					order.dispose();
				});
			});
	}

	private void updateStock(List<Product> products, List<Integer> quantity) {
		for(int i = 0; i < products.size(); i++) {
			products.get(i).setQuantity(products.get(i).getQuantity() - quantity.get(i));
			productBLL.updateProduct(products.get(i));
		}
	}
	
	private void writeResult(Orders order) {
			PrintWriter writer;
			try {
				writer = new PrintWriter(System.getProperty("user.dir") + "\\src\\main\\java\\bills\\" + order.getCustomerFirstName() +
										order.getCustomerLastName() + order.getId() + ".txt", "UTF-8");
				writer.println(order.toString());
				writer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
	}
	
	private List<String> getCustomerNames(){
		ArrayList<Customer> customers = (ArrayList<Customer>) customerBLL.findAllCustomers();
		List<String> customerNames = new ArrayList<>();
		for(Customer customer : customers) {
			customerNames.add(customer.getId() + " " + customer.getFirstName() + " " + customer.getLastName());
		}
		return customerNames;
	}
	
	private List<String> getProductNames(){
		ArrayList<Product> products = (ArrayList<Product>) productBLL.findAllProducts();
		List<String> productNames = new ArrayList<>();
		for(Product product : products) { 
			productNames.add(product.getId() + " " + product.getName() + " " + product.getPrice());
		}
		return productNames;
	}

	private String[] createHeader(List<Object> objects) {
		ArrayList<String> columns = new ArrayList<>();
		Field[] fields= objects.get(0).getClass().getDeclaredFields();
		for (Field field : fields) {
			columns.add(field.getName());
		}
		String[] header = new String[columns.size()];
		header = columns.toArray(header);
		return header;
	}

	private Object[][] createData(List<Object> objects) {
		Field[] fields = objects.get(0).getClass().getDeclaredFields();
		ArrayList<Object[]> rows = new ArrayList<>();
		Object value;
		for (Object obj : objects) {
			ArrayList<String> rowEntry = new ArrayList<>();
			for (Field field : fields) {
				field.setAccessible(true);
				try {
					value = field.get(obj);
					rowEntry.add(value.toString());
				} catch (IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			rows.add(rowEntry.toArray());
		}
		Object[][] data = new Object[rows.size()][rows.get(0).length];
		data = rows.toArray(data);
		return data;
	}

	public TableModel createTable(List<Object> objects) {
		String[] columns = createHeader(objects);
		Object[][] rows = createData(objects);
		return new DefaultTableModel(rows, columns) {
			private static final long serialVersionUID = 6438503399544238081L;

			@Override 
		    public boolean isCellEditable(int row, int column){
		        return column != 0;
		    }
		};
	}
}
