package presentation;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.TableModel;

import presentation.panels.ClientView;
import presentation.panels.OrderView;
import presentation.panels.ProductView;

public class View extends JFrame{
	private static final long serialVersionUID = 7285406992685996944L;
	private ClientView clients;
	private ProductView products;
	private OrderView orders;
	private JPanel upPanel;
	
	public View() {
		init();
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
		upPanel.setLayout(new BoxLayout(upPanel, BoxLayout.X_AXIS));
		upPanel.add(Box.createHorizontalGlue());
		upPanel.add(clients);
		upPanel.add(Box.createRigidArea(new Dimension(100,0)));
		upPanel.add(products);
		upPanel.add(Box.createHorizontalGlue());
		this.add(Box.createVerticalGlue());
		this.add(Box.createRigidArea(new Dimension(100,100)));
		this.add(upPanel);
		this.add(orders);
		this.add(Box.createRigidArea(new Dimension(100,100)));
		this.add(Box.createVerticalGlue());
		this.setLocationRelativeTo(null);
		this.setTitle("App");
		this.setSize(1280,1024);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	private void init() {
		clients = new ClientView();
		products = new ProductView();
		orders = new OrderView();
		upPanel = new JPanel();
	}
	
	public void updateCustomerTable(TableModel model) {
		clients.updateClientsTable(model);
	}
	
	public void updateProductTable(TableModel model) {
		products.updateProductsTable(model);
	}
	
	public void updateOrderTable(TableModel model) {
		orders.updateOrdersTable(model);
	}
	
	public void addProductActionListener(ActionListener ac) {
		products.addProductActionListener(ac);
	}
	
	public void editProductActionListener(ActionListener ac) {
		products.addEditActionListener(ac);
	}
	
	public void deleteProductActionListener(ActionListener ac) {
		products.addDeleteActionListener(ac);
	}
	
	public void showAllProductsActionListener(ActionListener ac) {
		products.addShowAllActionListener(ac);
	}
	
	public void addCustomerActionListener(ActionListener ac) {
		clients.addClientActionListener(ac);
	}
	
	public void editCustomerActionListener(ActionListener ac) {
		clients.addEditActionListener(ac);
	}
	
	public void deleteCustomerActionListener(ActionListener ac) {
		clients.addDeleteActionListener(ac);
	}
	
	public void showAllCustomersActionListener(ActionListener ac) {
		clients.addShowAllActionListener(ac);
	}
	
	public void placeOrderActionListener(ActionListener ac) {
		orders.addOrderActionListener(ac);
	}

	public void showAllOrdersActionListener(ActionListener ac) {
		orders.addShowAllActionListener(ac);
	}
	
	public List<String> getSelectedCustomer(){
		return clients.getSelectedClient();
	}
	
	public List<String> getSelectedProduct(){
		return products.getSelectedProduct();
	}
	
	public void showDialogBox(String message) {
		JOptionPane.showMessageDialog(this, message);
	}
}
