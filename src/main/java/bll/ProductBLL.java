package bll;

import java.util.ArrayList;
import java.util.List;

import bll.validators.PriceValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Product;

public class ProductBLL {
	private List<Validator<Product>> validators;
	private ProductDAO productDAO;

	public ProductBLL() {
		validators = new ArrayList<>();
		validators.add(new PriceValidator());
		productDAO = new ProductDAO();
	}

	public boolean insertProduct(Product product) {
		for (Validator<Product> v : validators) {
			if(!v.validate(product))
				return false;
		}
		return productDAO.insert(product);
	}

	public boolean deleteProduct(int id) {
		return productDAO.delete(id);
	}

	public boolean updateProduct(Product newProduct) {
		for (Validator<Product> v : validators) {
			if(!v.validate(newProduct))
				return false;
		}
		return productDAO.update(newProduct);
	}

	public List<Product> findAllProducts() {
		return productDAO.findAll();
	}
	
	public Product findProductById(int id) {
		return productDAO.findById(id);
	}
}
