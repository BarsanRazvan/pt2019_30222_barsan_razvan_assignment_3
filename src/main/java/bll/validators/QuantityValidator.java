package bll.validators;


import java.util.List;

import model.Orders;
import model.OrderItems;

public class QuantityValidator implements Validator<Orders> {

	@Override
	public boolean validate(Orders order) {
		List<OrderItems> items = order.getItems();
		for(OrderItems item : items) {
			if (item.getQuantity() < 1)
				return false;
		}
		return true;
	}

}
