package bll.validators;

import model.Customer;

public class AgeValidator implements Validator<Customer>{

	@Override
	public boolean validate(Customer cust) {
		if(cust.getAge() < 18)
			return false;
		return true;
	}
	
}
