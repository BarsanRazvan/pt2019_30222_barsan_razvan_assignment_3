package bll.validators;

import model.Product;

public class PriceValidator implements Validator<Product>{

	@Override
	public boolean validate(Product product) {
		if (product.getPrice()<0){
			return false;
		}
		else return true;
	}
}
