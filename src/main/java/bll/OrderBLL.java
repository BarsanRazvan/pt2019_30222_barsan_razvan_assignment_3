package bll;

import java.util.ArrayList;
import java.util.List;

import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.Orders;

public class OrderBLL {
	private List<Validator<Orders>> validators;
	private OrderDAO orderDAO;

	public OrderBLL() {
		validators = new ArrayList<>();
		validators.add(new QuantityValidator());
		orderDAO = new OrderDAO();
	}

	public boolean placeOrder(Orders order) {
		for (Validator<Orders> v : validators) {
			v.validate(order);
		}
		return orderDAO.insert(order);
	}
	
	public List<Orders> findAllOrders(){
		return orderDAO.findAll();
	}
	
}
