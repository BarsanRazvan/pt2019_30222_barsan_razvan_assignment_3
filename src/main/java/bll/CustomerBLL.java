package bll;

import java.util.ArrayList;
import java.util.List;

import bll.validators.AgeValidator;
import bll.validators.EmailValidator;
import bll.validators.Validator;
import dao.CustomerDAO;
import model.Customer;

public class CustomerBLL {
	private List<Validator<Customer>> validators;
	private CustomerDAO customerDAO;

	public CustomerBLL() {
		validators = new ArrayList<Validator<Customer>>();
		validators.add(new EmailValidator());
		validators.add(new AgeValidator());
		customerDAO = new CustomerDAO();
	}

	public boolean insertCustomer(Customer customer) {
		for (Validator<Customer> v : validators) {
			if(!v.validate(customer))
				return false;
		}
		return customerDAO.insert(customer);
	}

	public boolean deleteCustomer(int id) {
		return customerDAO.delete(id);
	}

	public boolean updateCustomer(Customer newCustomer) {
		for (Validator<Customer> v : validators) {
			if(!v.validate(newCustomer))
				return false;
		}
		return customerDAO.update(newCustomer);
	}

	public List<Customer> findAllCustomers() {
		return customerDAO.findAll();
	}
	
	public Customer findCustomerById(int id) {
		return customerDAO.findById(id);
	}

}
