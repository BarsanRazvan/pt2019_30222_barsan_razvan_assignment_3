package connection;


import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;



public class DriverJDBC {
	private static final Logger log = Logger.getLogger(DriverJDBC.class.getName());
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String DBURL = "jdbc:mysql://localhost:3306/TP_ORDER_SYSTEM";
	private static final String USER = "root";
	private static final String PASSWORD = "root";
	
	private static DriverJDBC singleton = new DriverJDBC();
	
	private DriverJDBC() {
		try {
			Class.forName(DRIVER);
		} catch(ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	
	private Connection createConnection() {
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(DBURL, USER, PASSWORD);
		} catch (SQLException e) {
			log.log(Level.WARNING, "ERROR! Connection could not be established successfully");
		}
		return connection;
	}
	
	public static Connection getConnection() {
		return singleton.createConnection();
	}
	
	public static void close(Connection connection) {
		try {
			if(connection != null)
				connection.close();
		} catch (SQLException e) {
			log.log(Level.WARNING, "ERROR! Connection could not be closed successfully");
		}
	}
	
	public static void close(Statement statement) {
			try {
				if(statement != null)
					statement.close();
			} catch (SQLException e) {
				log.log(Level.WARNING, "ERROR! Statement could not be closed successfully");
			}
	}
	
	public static void close(ResultSet resultSet) {
		try {
			if(resultSet != null)
				resultSet.close();
		} catch (SQLException e) {
			log.log(Level.WARNING, "ERROR! ResultSet could not be closed successfully");
		}
	}
	
	
	
	
}
